package pl.wbobiatynski.cosmose.roombooking.model.dao;

import static org.junit.Assert.*;

import java.util.List;

import org.testng.annotations.Test;

import pl.wbobiatynski.cosmose.roombooking.model.dao.entities.Hotel;

import org.junit.Before;
import org.testng.annotations.DataProvider;


public class HotelTests {	

	@DataProvider (name = "InitHotels")
	static Object[][] initializeHotels() {		
		return new Object[][] {{1,1,1,"Sheraton",50, 10, 300},	
			{2,1,1,"Holiday Inn.", 50,	10,	 300}, 
			{3,2,2,"Sheraton", 50, 10,	300}	};
	}
				
	@Test (dataProvider = "InitHotels")
	public void initHotels(int idowner, int idtown, String name, int numberOfCars, int numberOfLevels, int numberOfRooms) {	
		Hotel initList = new Hotel(idowner, idtown, name, numberOfCars, numberOfLevels, numberOfRooms); 		
	}
}
