package pl.wbobiatynski.cosmose.roombooking.model.dao.entities;

import java.io.Serializable;
import javax.persistence.*;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;


/**
 * The persistent class for the guest database table.
 * 
 */
@Entity
@Table(name="guest")


@NamedQueries({

	@NamedQuery(name="Guest.findById", query="SELECT g FROM Guest g WHERE idguest = :idGuest"),
	@NamedQuery(name="Guest.findAll", query="SELECT g FROM Guest g ")
})




public class Guest extends RoomBookingEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int idguest;

	private String citizenship;

	private String firstname;

	private int howmanypersons;

	private int married;

	private String name;
	
	private static StandardServiceRegistry registry;
	private static SessionFactory sessionFactory;
	
	
	EntityManager em = getEntityManager("pl.wbobiatynski.cosmose.roombooking.model.dao.entities.Guest");
	
	@DataProvider (name = "initGuests")	
	public Object[][] initGuest () {
		return new Object[][] {{"Polish", "Wojciech", 2, 0, "Bobiatynski", 13}, 
			{"German", "Angela", 4, 1, "Merkel", 14}, {"American", "Bill", 5, 1, "Gates", 15}};		
	}
			
	@Factory (dataProvider = "initGuests") 
	void initializeHotels (Guest guest) {
		insertGuest(em, guest);		
	}	
			
	private void insertGuest(EntityManager em, Guest guest) {		
		em.persist(guest);
	}
	
	

	//bi-directional one-to-one association to Room
	@OneToOne
	@JoinColumn(name="idguest")
	private Room room;

	public Guest() {
	}

	
	
	
	public Guest(String citizenship, String firstname, int howmanypersons, int married, String name, int idRoom) {
		super();
		this.citizenship = citizenship;
		this.firstname = firstname;
		this.howmanypersons = howmanypersons;
		this.married = married;
		this.name = name;
		
		
		Session hibernateSession = sessionFactory.getCurrentSession();
		
		Transaction transaction = null;
		transaction = hibernateSession.beginTransaction();
		
		SQLQuery query = hibernateSession.createSQLQuery("Room.findById");
		query.setParameter("idRoom", idRoom);
		
		this.room = (Room) query.getQueryReturns();
	}
	
	
	public int getIdguest() {
		return this.idguest;
	}

	public void setIdguest(int idguest) {
		this.idguest = idguest;
	}

	public String getCitizenship() {
		return this.citizenship;
	}

	public void setCitizenship(String citizenship) {
		this.citizenship = citizenship;
	}

	public String getFirstname() {
		return this.firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public int getHowmanypersons() {
		return this.howmanypersons;
	}

	public void setHowmanypersons(int howmanypersons) {
		this.howmanypersons = howmanypersons;
	}

	public int getMarried() {
		return this.married;
	}

	public void setMarried(int married) {
		this.married = married;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Room getRoom() {
		return this.room;
	}

	public void setRoom(Room room) {
		this.room = room;
	}

}