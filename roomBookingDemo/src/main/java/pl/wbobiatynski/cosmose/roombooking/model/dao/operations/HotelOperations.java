package pl.wbobiatynski.cosmose.roombooking.model.dao.operations;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import pl.wbobiatynski.cosmose.roombooking.model.dao.entities.Hotel;

public class HotelOperations {
	
	public void insertRecords (List<Hotel> hotelsCollection) {
		 for (Hotel h : hotelsCollection) {
			 em.persist(h);	 			 
		 }		
	}	
	
	
	
}
