package pl.wbobiatynski.cosmose.roombooking.model.dao.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the town database table.
 * 
 */
@Entity
@NamedQuery(name="Town.findAll", query="SELECT t FROM Town t")
public class Town implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int idtown;

	private String name;

	//bi-directional many-to-one association to Country
	@ManyToOne
	@JoinColumn(name="idcountry", referencedColumnName="idcountry")
	private Country country;

	public Town() {
	}

	public int getIdtown() {
		return this.idtown;
	}

	public void setIdtown(int idtown) {
		this.idtown = idtown;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Country getCountry() {
		return this.country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}

}