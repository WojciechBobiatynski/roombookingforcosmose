package pl.wbobiatynski.cosmose.roombooking.model.dao.entities;

import java.io.Serializable;
import javax.persistence.*;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;

import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the country database table.
 * 
 */
@Entity
@NamedQuery(name="Country.findAll", query="SELECT c FROM Country c")
public class Country extends RoomBookingEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private String code;

	private int capital;

	private String code2;

	private String continent;

	private String country;

	@Column(name="country_id")
	private int countryId;

	private float gnp;

	private float GNPOld;

	private String governmentForm;

	private String headOfState;

	private short indepYear;

	@Column(name="last_update")
	private Timestamp lastUpdate;

	private float lifeExpectancy;

	private String localName;

	private String name;


	private int population;

	private String region;

	private float surfaceArea;

	
	EntityManager em = getEntityManager(Country.class.getCanonicalName());

	
	
	@DataProvider (name = "initCountry")	
	public Object[][] initCountries () {
		return new Object[][] {{1, 1, 2, 2, 3, 45, 3, 2, 150, "East"}, 
			{2, 4, 2, 2, 3, 48, 3, 3, 158, "West"}, {3, 4, 2, 2, 3, 69, 3, 3, 157, "South"}};		
	}
			
	@Factory (dataProvider = "initCountries") 
	void initializeRooms (Country country) {
		insertCountry(country);		
	}	
	
	private void insertCountry(Country country) {		
		em.persist(country);
	}
	
			

	//bi-directional many-to-one association to Town
	@OneToMany(mappedBy="country")
	private List<Town> towns;

	public Country() {
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public int getCapital() {
		return this.capital;
	}

	public void setCapital(int capital) {
		this.capital = capital;
	}

	public String getCode2() {
		return this.code2;
	}

	public void setCode2(String code2) {
		this.code2 = code2;
	}

	public String getContinent() {
		return this.continent;
	}

	public void setContinent(String continent) {
		this.continent = continent;
	}

	public String getCountry() {
		return this.country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public int getCountryId() {
		return this.countryId;
	}

	public void setCountryId(int countryId) {
		this.countryId = countryId;
	}

	public float getGnp() {
		return this.gnp;
	}

	public void setGnp(float gnp) {
		this.gnp = gnp;
	}

	public float getGNPOld() {
		return this.GNPOld;
	}

	public void setGNPOld(float GNPOld) {
		this.GNPOld = GNPOld;
	}

	public String getGovernmentForm() {
		return this.governmentForm;
	}

	public void setGovernmentForm(String governmentForm) {
		this.governmentForm = governmentForm;
	}

	public String getHeadOfState() {
		return this.headOfState;
	}

	public void setHeadOfState(String headOfState) {
		this.headOfState = headOfState;
	}

	public short getIndepYear() {
		return this.indepYear;
	}

	public void setIndepYear(short indepYear) {
		this.indepYear = indepYear;
	}

	public Timestamp getLastUpdate() {
		return this.lastUpdate;
	}

	public void setLastUpdate(Timestamp lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	public float getLifeExpectancy() {
		return this.lifeExpectancy;
	}

	public void setLifeExpectancy(float lifeExpectancy) {
		this.lifeExpectancy = lifeExpectancy;
	}

	public String getLocalName() {
		return this.localName;
	}

	public void setLocalName(String localName) {
		this.localName = localName;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}



	public int getPopulation() {
		return this.population;
	}

	public void setPopulation(int population) {
		this.population = population;
	}

	public String getRegion() {
		return this.region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public float getSurfaceArea() {
		return this.surfaceArea;
	}

	public void setSurfaceArea(float surfaceArea) {
		this.surfaceArea = surfaceArea;
	}

	public List<Town> getTowns() {
		return this.towns;
	}

	public void setTowns(List<Town> towns) {
		this.towns = towns;
	}

	public Town addTown(Town town) {
		getTowns().add(town);
		town.setCountry(this);

		return town;
	}

	public Town removeTown(Town town) {
		getTowns().remove(town);
		town.setCountry(null);

		return town;
	}

}