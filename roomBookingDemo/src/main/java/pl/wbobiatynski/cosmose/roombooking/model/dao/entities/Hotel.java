package pl.wbobiatynski.cosmose.roombooking.model.dao.entities;

import java.io.Serializable;
import javax.persistence.*;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;


/**
 * The persistent class for the hotel database table.
 * 
 */
@Entity
@NamedQuery(name="Hotel.findAll", query="SELECT h FROM Hotel h")
public class Hotel extends RoomBookingEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int idhotel;

	private int idowner;

	private int idtown;

	private String name;

	private int numberofcars;

	private int numberoflevels;

	private int numberofrooms;
	
	EntityManager em = getEntityManager(this.getClass().getCanonicalName());
			
	@DataProvider (name = "InitHotels")
	static Object[][] initializeHotels() {		
		return new Object[][] {{ 1, 1, "Sheraton", 50, 10, 300},	
			{1,1,"Holiday Inn.", 50, 10, 300}, 
			{2,2,"Sheraton", 50, 10,300}};
	}
	
	
	
	public Hotel(int idowner, int idtown, String name, int numberofcars, int numberoflevels,
			int numberofrooms) {
		super();		
		this.idowner = idowner;
		this.idtown = idtown;
		this.name = name;
		this.numberofcars = numberofcars;
		this.numberoflevels = numberoflevels;
		this.numberofrooms = numberofrooms;
	}
	
	
	@Factory (dataProvider = "InitHotels")
	public void initHotels(Hotel h) {
		insertHotel(h);						
	}		
		
	private void insertHotel(Hotel hotel) {
		em.persist(hotel);		
	}

	public Hotel() {
	}
	
	public int getIdhotel() {
		return this.idhotel;
	}

	public void setIdhotel(int idhotel) {
		this.idhotel = idhotel;
	}

	public int getIdowner() {
		return this.idowner;
	}

	public void setIdowner(int idowner) {
		this.idowner = idowner;
	}

	public int getIdtown() {
		return this.idtown;
	}

	public void setIdtown(int idtown) {
		this.idtown = idtown;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getNumberofcars() {
		return this.numberofcars;
	}

	public void setNumberofcars(int numberofcars) {
		this.numberofcars = numberofcars;
	}

	public int getNumberoflevels() {
		return this.numberoflevels;
	}

	public void setNumberoflevels(int numberoflevels) {
		this.numberoflevels = numberoflevels;
	}

	public int getNumberofrooms() {
		return this.numberofrooms;
	}

	public void setNumberofrooms(int numberofrooms) {
		this.numberofrooms = numberofrooms;
	}

}