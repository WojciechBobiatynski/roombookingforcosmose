package pl.wbobiatynski.cosmose.roombooking.model.dao.entities;

import java.io.Serializable;
import javax.persistence.*;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;


/**
 * The persistent class for the owner database table.
 * 
 */
@Entity
@NamedQuery(name="Owner.findAll", query="SELECT o FROM Owner o")
public class Owner extends RoomBookingEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int idowner;

	EntityManager em = getEntityManager("pl.wbobiatynski.cosmose.roombooking.model.dao.entities.Owner");
		
	@DataProvider (name = "initOwners")	
	public Object[][] initOwners () {
		return new Object[][] {{"Golebiewski"}, {"Carrington"}, {"Anderson"}};		
	}
			
	public Owner(String name) {
		this.name = name;
	}

	@Factory (dataProvider = "initHotels") 
	void initializeHotels (Owner owner) {
		insertOwner(em, owner);		
	}	
			
	private void insertOwner(EntityManager em, Owner owner) {		
		em.persist(owner);
	}
	
	private String name;

	public Owner() {
	}

	public int getIdowner() {
		return this.idowner;
	}

	public void setIdowner(int idowner) {
		this.idowner = idowner;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

}