package pl.wbobiatynski.cosmose.roombooking.model.dao.entities;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class RoomBookingEntity {

	protected EntityManager getEntityManager (String entityClassName) {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory(entityClassName);
		return emf.createEntityManager();		
	}	
}
