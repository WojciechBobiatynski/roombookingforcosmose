package pl.wbobiatynski.cosmose.roombooking.model.dao.entities;

import java.io.Serializable;
import javax.persistence.*;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;

import pl.wbobiatynski.epoint.model.Product;


/**
 * The persistent class for the room database table.
 * 
 */
@Entity
@NamedQuery(name="Room.findById", query="SELECT r FROM Room r WHERE idroom = :idRoom")

public class Room extends RoomBookingEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	EntityManager em = getEntityManager(Room.class.getCanonicalName());
		
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int idroom;

	@ManyToOne
	@JoinColumn (name = "idhotel")	
	private int idhotel;
	
	private int level;

	private int numberofbathrooms;

	private int numberofbeds;

	private int numberofeldevices;

	private int numberofmeters;

	private int numberofpersons;

	private int numberofrooms;

	private int priceperday;

	private String worldside;
	
	private static StandardServiceRegistry registry;
	private static SessionFactory sessionFactory;

		
	@DataProvider (name = "initRoom")	
	public Object[][] initRooms () {
		return new Object[][] {{1, 1, 2, 2, 3, 45, 3, 2, 150, "East"}, 
			{2, 4, 2, 2, 3, 48, 3, 3, 158, "West"}, {3, 4, 2, 2, 3, 69, 3, 3, 157, "South"}};		
	}
			
	@Factory (dataProvider = "initRooms") 
	void initializeRooms (Room room) {
		insertRoom(em, room);		
	}	
	
	private void insertRoom(EntityManager em, Room room) {		
		em.persist(room);
	}
	
	
	//bi-directional one-to-one association to Room
	@OneToOne
	@JoinColumn(name="idroom")
	private Guest guest;


	
		
	public Room(int idhotel, int level, int numberofbathrooms, int numberofbeds, int numberofeldevices,
			int numberofmeters, int numberofpersons, int numberofrooms, int priceperday, String worldside,
			Guest guest) {
		super();
		this.idhotel = idhotel;
		this.level = level;
		this.numberofbathrooms = numberofbathrooms;
		this.numberofbeds = numberofbeds;
		this.numberofeldevices = numberofeldevices;
		this.numberofmeters = numberofmeters;
		this.numberofpersons = numberofpersons;
		this.numberofrooms = numberofrooms;
		this.priceperday = priceperday;
		this.worldside = worldside;
		this.guest = guest;
	}
	
		
	
	public Room() {
	}

	public int getIdroom() {
		return this.idroom;
	}

	public void setIdroom(int idroom) {
		this.idroom = idroom;
	}

	public int getIdhotel() {
		return this.idhotel;
	}

	public void setIdhotel(int idhotel) {
		this.idhotel = idhotel;
	}

	public int getLevel() {
		return this.level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public int getNumberofbathrooms() {
		return this.numberofbathrooms;
	}

	public void setNumberofbathrooms(int numberofbathrooms) {
		this.numberofbathrooms = numberofbathrooms;
	}

	public int getNumberofbeds() {
		return this.numberofbeds;
	}

	public void setNumberofbeds(int numberofbeds) {
		this.numberofbeds = numberofbeds;
	}

	public int getNumberofeldevices() {
		return this.numberofeldevices;
	}

	public void setNumberofeldevices(int numberofeldevices) {
		this.numberofeldevices = numberofeldevices;
	}

	public int getNumberofmeters() {
		return this.numberofmeters;
	}

	public void setNumberofmeters(int numberofmeters) {
		this.numberofmeters = numberofmeters;
	}

	public int getNumberofpersons() {
		return this.numberofpersons;
	}

	public void setNumberofpersons(int numberofpersons) {
		this.numberofpersons = numberofpersons;
	}

	public int getNumberofrooms() {
		return this.numberofrooms;
	}

	public void setNumberofrooms(int numberofrooms) {
		this.numberofrooms = numberofrooms;
	}

	public int getPriceperday() {
		return this.priceperday;
	}

	public void setPriceperday(int priceperday) {
		this.priceperday = priceperday;
	}

	public String getWorldside() {
		return this.worldside;
	}

	public void setWorldside(String worldside) {
		this.worldside = worldside;
	}
	
	Room findById (int idRoom) {
		  em.getTransaction().begin();
		  Room room = em.find(Room.class, idRoom);
		  em.getTransaction().commit();
		  return room;
		}
	}	
	

